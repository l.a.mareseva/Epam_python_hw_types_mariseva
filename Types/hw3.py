'''
1. После запуска предлагает пользователю ввести целые неотрицательные числа,
разделенные любым не цифровым литералом (пробел, запятая, %, буква и т.д.)
и циклически ожидает ввода от пользователя.
2. Получив вводные данные, выделяет полученные числа, суммирует их,
и печатает полученную сумму. После чего ожидает следующего ввода.
3. При получении в качестве вводных данных 'cancel' завершает свою работу.
'''

while True:
    print('Введите целые неотрицательные числа, разделенные любым не цифровым литералом:')
    message = str(input())
    if message == 'cancel':
        print('Bye!')
        break
    else:
        message = [str(i) for i in message]
        sum = 0
        curent = 0
        for i in range(0, len(message)):
            if message[i].isdigit():
                curent = curent*10 + int(message[i])
                if i == len(message)-1:
                    sum += curent
            else:
                sum += curent
                curent = 0
        print(sum)
