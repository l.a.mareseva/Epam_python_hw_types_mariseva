'''1. После запуска предлагает пользователю ввести текст.
2. В качестве ответа печатает наиболее часто встречающееся в тексте слово
или несколько таких слов, если имеет место "ничья". Также указывая
сколько именно раз слово встретилось в тексте. (Игнорируйте заглавные буквы
при отождествлении слов - то есть считайте слова "Подлодка" и "подлодка"
одинаковыми, а разные формы слов - разными словами)
После чего ждет следующего ввода.
3.При получении в качестве вводных данных 'cancel' завершает свою работу.'''

while True:
    print('Введите текст:')
    message = [str(i) for i in input().split()]
    if message[0] == 'cancel':
        print('Bye!')
        break
    else:
        words = {}
        max = 0
        for i in range(0, len(message)):
            if message[i] in words.keys():
                words[message[i]] += 1
            else:
                words.update({message[i]: 1})
            if words[message[i]] > max:
                max = words[message[i]]
        for i in words.keys():
            if words[i] == max:
                print(i, ' - ', words[i])


