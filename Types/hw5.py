'''
The decimal number, 585 = 1001001001 in binary, is palindromic in both bases.
Find the sum of all numbers, less than one million, which are palindromic in
base 10 and base 2. (Please note that the palindromic number,
in either base, may not include leading zeros.)

'''
palindromes = []
for i in range(1, 1000000):
    dec = list(str(i))
    bi = list(str(bin(i)))
    bi = bi[2:]
    if (dec == dec[::-1] and bi == bi[::-1]):
        palindromes.append(i)
print('все числа, что полиндромы в десятичной и двоичной системе, дают в сумме ', sum(palindromes))
