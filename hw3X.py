'''
*Дополнительный уровень сложности:
Пусть программа может принимать числа разделенные любыми не цифровыми символами
в любом количестве. Иными словами, из любой введенной строки выделяет числа
и суммирует их. Пусть также числа могут быть отрицательными. То есть считайте,
что если перед числом стоит минус, то он указывает на отрицательность числа.

Например:

-> 123dfgdr%0&45ty-45--900
-777


'''
'''
while True:
    print('Введите целые числа, разделенные любым не цифровым литералом:')
    message = [str(i) for i in input()]
    if (message[0] == 'c' and message[1] == 'a' and message[2] == 'n' and message[3] == 'c'
        and message[4] == 'e' and message[5] == 'l'):
        print('Bye!')
        break
    else:
        nums = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}
        sum = 0
        curent = 0
        for i in range(0, len(message)):
            if (message[i]) in nums:
                if (i > 0 and (message[i - 1] == '-')) or (curent < 0):
                    curent = curent * 10 - int(message[i])
                else:
                    curent = curent * 10 + int(message[i])
                if i == len(message)-1:
                    sum += curent
            else:
                sum += curent
                curent = 0
        print(sum) '''
while True:
    print('Введите целые неотрицательные числа, разделенные любым не цифровым литералом:')
    message = str(input())
    if message == 'cancel':
        print('Bye!')
        break
    else:
        message = [str(i) for i in message]
        sum = 0
        curent = 0
        for i in range(0, len(message)):
            if message[i].isdigit():
                if (i > 0 and (message[i - 1] == '-')) or (curent < 0):
                    curent = curent * 10 - int(message[i])
                else:
                    curent = curent * 10 + int(message[i])
                if i == len(message) - 1:
                    sum += curent
            else:
                sum += curent
                curent = 0
        print(sum)