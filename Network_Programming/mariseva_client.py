from socket import AF_INET, SOCK_STREAM, socket
from threading import Thread, Event
import time

addr = ('localhost', 50000)
s = socket(AF_INET, SOCK_STREAM)
event = Event()


def write():
    while not event.is_set():
        send_message(s, input())


def receive():
    while not event.is_set():
        msg = receive_message(s)
        if msg == 'q':
            s.close()
            event.set()
        else:
            print(msg)


def receive_message(client, buffer_size: int=1024, decode: str='utf8') -> str:
    return client.recv(buffer_size).decode(decode)


def send_message(client, text: str='test', encoding: str='utf8'):
    client.send(bytes(text, encoding))


if __name__ == '__main__':
    s.connect(addr)
    writer = Thread(name='writer', target=write, daemon=True)
    receiver = Thread(name='receiver', target=receive, daemon=True)
    writer.start()
    receiver.start()
    while not event.is_set():
        time.sleep(.1)
