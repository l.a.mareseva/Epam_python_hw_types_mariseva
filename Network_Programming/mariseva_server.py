from socket import *
from threading import *

clients = {}
addresses = {}
addr = ('localhost', 50000)
s = socket(AF_INET, SOCK_STREAM)
s.bind(addr)


def accept_incoming_connections():
    while True:
        client, client_address = s.accept()
        print('{} connected.'.format(client_address))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client, client_address,)).start()


def handle_client(c, c_addr):
    name = registration(c, c_addr)
    if name not in ('q', ''):
        print('received name is {}'.format(name))
        while True:
            msg = receive_message(c)
            if msg == 'p':
                private(c, name)
            elif msg == 'u':
                users(c)
            elif msg == 'q':
                quit_from_chat(c, name)
                break
            else:
                send_to_all(msg, name + ': ')
    else:
        send_message(c, 'q')
        c.close()
        del addresses[c]
        print('{} has left the chat.'.format(c_addr))


def registration(c, client_address) -> str:
    send_message(c, 'Welcome to the club, buddy! Pls, enter your nickname: ')
    name = receive_message(c)
    if name != 'q':
        while name in clients.keys():
            send_message(c, 'Nope, try again...')
            name = receive_message(c)
        send_message(c, 'Greetings,  {}! List of commands: q-exit, u-list of users, p-private message.'.format(name))
        send_to_all('{} has joined the chat!'.format(name))
        clients[name] = c
    return name


def private(c, name):
    send_message(c, 'Choose user to whisper: ')
    send_message(c, ' '.join(clients.keys()))
    friend = c.recv(1024).decode('utf8')
    if friend in clients.keys():
        send_message(c, 'Enter your whisper to {}: '.format(friend))
        whisper = receive_message(c)
        send_message(c, 'whisper to {}: {}'.format(friend, whisper))
        send_message(clients[friend], 'whisper from {}: '.format(name) + whisper)
    else:
        send_message(c, 'There\'s no {} user in chat.'.format(name))


def users(c):
    send_message(c, ' '.join(clients.keys()))


def quit_from_chat(c, name: str):
    send_message(c, 'q')
    c.close()
    del clients[name]
    del addresses[c]
    send_to_all('{} has left the chat.'.format(name))
    print('{} has left the chat.'.format(name))


def send_to_all(msg, name=''):
    for c in clients.values():
        send_message(c, name + msg)


def receive_message(client, buffer_size: int=1024, decode: str='utf8') -> str:
    return client.recv(buffer_size).decode(decode)


def send_message(client, text: str='test', encoding: str='utf8'):
    client.send(bytes(text, encoding))


if __name__ == '__main__':
    s.listen(10)
    print('Waiting for connection...')
    main_worker = Thread(target=accept_incoming_connections)
    main_worker.start()
    main_worker.join()
    s.close()
