import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException


class AutoLike:

    def __init__(self, login, password):
        self.driver = webdriver.Chrome() #удален прямой адресс
        self.login = login
        self.password = password        
        self.driver.set_window_size(1920,1080)

    def root_process(self):
        self.facebook_login()
        if self.tinder_login():
            self.swipes()
        self.driver.close()

    def facebook_login(self):
        self.driver.get('http://facebook.com')
        self.driver.find_element_by_id('email').send_keys(self.login)
        self.driver.find_element_by_id('pass').send_keys(self.password)
        self.driver.find_element_by_id('loginbutton').click()
        print('Facebook_login ✓')

    def tinder_login(self):

        self.driver.get('http://tinder.com')
        time.sleep(5)
        self.driver.find_element_by_xpath(
            "//*[@id='modal-manager']/div/div/div[2]/div/div[3]/div[1]/button/span/span").click()
        print('b1')
        time.sleep(5)
        try:
            self.driver.find_element_by_xpath(
                "//*[@id=\"content\"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]/span/span").click()
            print('b2')
            time.sleep(5)
            self.driver.find_element_by_xpath(
                "//*[@id=\"content\"]/div/span/div/div[2]/div/div/main/div/div[3]/button/span/span").click()
            print('b3')
            time.sleep(5)
            self.driver.find_element_by_xpath(
                "//*[@id=\"content\"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]/span/span").click()
            print('b4')
            time.sleep(5)
            self.driver.find_element_by_xpath(
                "//*[@id=\"content\"]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]/span/span").click()
            print('b5')
            time.sleep(1)
        except:
            print('Упс, что-то пошло не так')
            return False
        print('tinder_login ✓')
        return True


    def swipes(self):

        try:
            while True:
                self.driver.find_element_by_xpath('/html/body').send_keys(Keys.ARROW_RIGHT)
                time.sleep(1)
                try:
                    if self.driver.find_element_by_xpath('/html/body/div[2]/div/div/div[3]/button[1]'):
                        break
                except NoSuchElementException:
                    pass
        except Exception as e:
            print('Упс, что-то пошло не так')
            raise e



if __name__ == "__main__":
    str1, str2 = 'Автолайкер использует авторизацию в тиндере через Facebook\n Для дальнейшей работы введите логин для Facebook:\n', 'и пароль: \n'
    like_it = AutoLike(input(str1), input(str2))
    like_it.root_process()

