import math


def side_len(x, y):
    return math.sqrt((y[0] - x[0])**2+(y[1] - x[1])**2)


def square(a, b, c):
    right, left, up, down = max(a[0], b[0], c[0]), min(a[0], b[0], c[0]),\
                              max(a[1], b[1], c[1]), min(a[1], b[1], c[1])

    data = []
    kol = up - down + 2
    line = [k for k in range(left, right+1)]
    column = [k for k in range(down, up+1)]
    for i in range(0, len(line)):
        for j in range(0, len(column)):
            data.append((line[i], column[j]))
    return data


def area(a, b, c):
    p = (a+b+c)/2
    return math.sqrt(p*(p-a)*(p-b)*(p-c))


def count_points(first, second, third):
    count = 0
    matrix = square(first, second, third)
    '#сумма площадей  AKB, AKC, BKC > площади  ABC - точка лежит вне треугольника'
    for i in range(0, len(matrix)):
        e = matrix[i]
        ab, bc, ca = side_len(first, second), side_len(second, third), side_len(third, first)
        ae, eb, ec = side_len(first, e), side_len(second, e), side_len(third, e)
        s_triangle = area(ab, bc, ca)
        s1, s2, s3 = area(ae, eb, ab), area(ca, ec, ae), area(eb, bc, ec)
        if round(s_triangle, 1) >= round((s1+s2+s3), 1):
            count += 1
    #S=В+Г/2-1
    return count

print(count_points((5,2),(1,3),(-1,-3)))