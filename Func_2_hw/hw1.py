def make_number(lst, acc=0, i=0):
    if i < len(lst) and (not lst[i].isdigit()):
        return
    elif len(lst) == i:
        return acc
    else:
        return make_number(lst, acc*10+int(lst[i]), i+1)


while True:
    print('Введите текст:')
    message = str(input())
    if message == 'cancel':
        print('Bye!')
        break
    else:
        message = [str(i) for i in message]
        result = make_number(message)
        if result is None:
            print('Не удалось преобразовать введенный текст в число.')
        else:
            if result % 2 == 0:
                print(int(result/2))
            else:
                print(result*3+1)
