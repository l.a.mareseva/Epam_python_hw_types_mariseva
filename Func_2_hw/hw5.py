'''
Проверить при помощи этой функции время исполнения различных
алгоритмов поиска чисел фиббоначи.
Найти наиболее оптимальный метод.

'''
from __future__ import division
import math
import time
from timeit import *
import sys
import functools

calls1 = [0, 0.0, 0]
calls2 = [0, 0.0, 0]
calls3 = [0, 0.0, 0]
calls4 = [0, 0.0, 0]

M = {0: 0, 1: 1}

def make_cache1(function):
    cache = {}

    @functools.wraps(function)
    def wrapper(*args):
        if args in cache:
            return cache[args]
        else:
            val = function(*args)
            cache[args] = val
            return val
    return wrapper


def my_decor(name):
    def decor_for_func(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            globals()[name][0] += 1
            start = default_timer()
            v = function(*args, **kwargs)
            stop = default_timer()
            globals()[name][1] += (stop-start)
            globals()[name][2] = function.__name__
            return v
        return wrapper
    return decor_for_func


@my_decor('calls1')
@make_cache1
def fib1(n):
    fib1.__name__ = 'fib1'
    if n in (1, 2):
        return 1
    return fib1(n-1) + fib1(n-2)


@my_decor('calls2')
def fib2(n):
    if n in M:
        return M[n]
    M[n] = fib2(n - 1) + fib2(n - 2)
    return M[n]


@my_decor('calls3')
def fib3(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a


def pow(x, n, I, mult):

    if n == 0:
        return I
    elif n == 1:
        return x
    else:
        y = pow(x, n // 2, I, mult)
        y = mult(y, y)
        if n % 2:
            y = mult(x, y)
        return y


def identity_matrix(n):
    """Возвращает единичную матрицу n на n"""
    r = list(range(n))
    return [[1 if i == j else 0 for i in r] for j in r]


def matrix_multiply(A, B):
    BT = list(zip(*B))
    return [[sum(a * b
                 for a, b in zip(row_a, col_b))
            for col_b in BT]
            for row_a in A]


@my_decor('calls4')
def fib4(n):
    F = pow([[1, 1], [1, 0]], n, identity_matrix(2), matrix_multiply)
    return F[0][1]

item = 300
#acceleration
for i in range(0, 200):
    fib1(item)
    fib2(item)
    item+=300
print(fib1(item))
print(fib2(item))
print(fib3(item))
print(fib4(item))

lst = [calls1, calls2, calls3, calls4]
print('results:\n', lst)
m = [sys.maxsize, 0]
for i in lst:
    if i[1] < m[0]:
        m[0] = i[1]
        m[1] = i[2]
print('The best is ', m[1], 'because it need ', m[0], 'seconds')


