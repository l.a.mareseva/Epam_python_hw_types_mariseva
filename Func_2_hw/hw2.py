import locale
from math import *
locale.setlocale(locale.LC_ALL, "")


def get_l(a, v):
    if len(str(a)) == 1:
        return 1 # 0.001
    elif a > 0.01:
        if a > v:
            if v > 0.00001:
                return len(str(v))-2
            else:
                v = locale.format("%f", v)
                return len(str(v)) - 2
        else:
            return len(str(a)) - 2
    elif a > 0.00001:
        return len(str(a))-2
    else:
        a = locale.format("%f", a)
        return len(str(a))-2


def mysqrt(value, accuracy=0.1):
    def correction():
        if abs(current[1] - current[0]) <= accuracy and current[1] != 0:
            return round(current[0], l)
        else:
            current[1], current[0] = current[0], (current[0] + value/current[0]) / 2
            return correction()
    current = [value / 2, 0]
    l, le = get_l(accuracy, value) + 2, len(str(accuracy)) - 2
    if l > le:
        for i in range(0, abs(le - l)):
            accuracy *= 0.1
    return correction()


print(mysqrt(15))
print(mysqrt(4/10000, 0.0001))

print(mysqrt(4/10000, 0.01))
print(mysqrt(4/1000000, 0.01))
print(mysqrt(4/100000000, 0.01))
