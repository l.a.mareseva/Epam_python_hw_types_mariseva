import time
calls =[]
def make_cache(limit):
    cache = {}

    timers = {}
    def f_cache(function):

        '''def wrapper(*args):
            nonlocal cache
            real_cashe = cache.values
            print(type(real_cashe))
            if args in real_cashe:
                for item in cache.keys():
                    if time.time() - item < limit:
                        cache.pop(item)
                real_cashe = cache.values
                return real_cashe[args]
            else:
                val = function(*args)
                new = [time.time(), {[args]: val}]
                cache.update(new)
                return val'''
        def wrapper(*args):
            nonlocal cache
            timer = list(timers)
            for item in timer:
                if time.time() - item > limit:
                    cache.pop(timers.pop(item))
            if args in cache:
                return cache[args]
            else:
                val = function(*args)
                cache[args] = val
                timers[time.time()] = args
                return val

        return wrapper
    return f_cache


@make_cache(5)
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)


print(fib(20))
time.sleep(3)
print(fib(10))
time.sleep(2)


