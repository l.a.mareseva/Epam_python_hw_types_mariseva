import subprocess
import psutil


def process_count(username: str) -> int:
    n_process = 0
    for pid in psutil.pids():
        process = psutil.Process(pid)
        if  process.username() == username:
            n_process += 1
    return n_process


def total_memory_usage(root_pid: int) -> int:
    process = psutil.Process()
    new = process.parent()
    return new.memory_info()[0]

print(process_count("root"))
print(total_memory_usage(1))