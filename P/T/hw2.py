import threading
import time
sem = threading.Semaphore()

class StoppableThread(threading.Thread):

    def __init__(self, target):
        super(StoppableThread, self).__init__()
        self._stop_event = threading.Event()
        self.target = target
        self.working = True

    def stop(self):
        self.working = False

    def run(self):
        while self.working:
            self.target()
            pass

def fun1():
        sem.acquire()
        print(1)
        sem.release()
        time.sleep(0.25)

def fun2():
        sem.acquire()
        print(2)
        sem.release()
        time.sleep(0.25)


t1 = StoppableThread(target = fun1)
t1.daemon = True
t2 = StoppableThread(target = fun2)
t2.daemon = True
t1.start()
t2.start()
while True:
    try:
        time.sleep(1)
    except KeyboardInterrupt as e:
        t1.stop()
        t2.stop()
        break