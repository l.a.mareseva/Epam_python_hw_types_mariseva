'''
Нужно реализовать контекстный менеджер pidfile
Данный декоратор должен не позволять запускать параллельно больше одного блока кода,
    завёрнутого в данный декоратор
'''

import time
import os
import sys


class pidfile:

    def __init__(self, file_name: str):
        self.file_name = file_name
        if os.path.isfile(self.file_name):
            pid_list = []
            for pid in os.listdir('/proc'):
                if pid.isdigit():
                    pid_list.append(pid)
            file = open(self.file_name, 'r')
            for line in file:
                if line in pid_list:
                    file.close()
                    raise ConnectionRefusedError('Pid already in use')
                else:
                    f = open(self.file_name, 'w')
                    f.write(str(os.getpid()))
                    f.close()
        else:
            file = open(self.file_name, 'w')
            file.write(str(os.getpid()))
            file.close()

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        os.remove(self.file_name)


''''
fn = "C:\\Users\lamar\Desktop\\test.txt"
with pidfile(fn):
    print('directory ', fn)
    time.sleep(10)
print('done')'''