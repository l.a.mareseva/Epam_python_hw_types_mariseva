'''
Нужно написать декоратор, который завернёт функцию в контекст менеджер.
Контекст менеджер должен пененаправлять stderr в указанный файл.
Если dest is None, то перенаправление осуществляется в stdout, иначе dest - строка,
    сожержащая путь до файла, куда осуществляется перенаправление
'''


import sys


class ContextManager:
    def __init__(self, func, dest, *args, **kwargs):
        self.func = func
        self.dest = dest
        self.args = args
        self.kwargs = kwargs
        self.normal_stderr = sys.stderr

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        # вернуть нормальный stderr
        sys.stderr = self.normal_stderr

    def diversion(self):
        f = 0
        if self.dest is None:
            # перенаправление в stdout
            sys.stderr = sys.stdout
        else:
            file = open(self.dest, 'a')
            sys.stderr = file
            f = 1
        try:
            return self.func(*self.args, **self.kwargs)
        except Exception as ex:
            sys.stderr.write(str(ex))
            if f:
                file.close()


def stderr_redirect(dest=None):

    def my_decorator(func):
        def wrapped(*args, **kwargs):
            with ContextManager(func, dest, *args, **kwargs) as cm:
                return cm.diversion()
        return wrapped
    return my_decorator


@stderr_redirect(dest=None)
def test():
    sys.stderr.write('stderr text ')
    return 0


print(test())
