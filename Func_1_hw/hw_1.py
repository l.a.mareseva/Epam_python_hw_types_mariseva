'''
partial, принимает функцию func, произвольный набор позиционных fixated_args
и именованных fixated_kwargs аргументов и возвращет новую функцию,
которая обладает следующими свойствами:

1.При вызове без аргументов повторяет поведение функции func, вызванной
с fixated_args и fixated_kwargs.
2.При вызове с позиционными и именованными аргументами дополняет ими
fixated_args (приписывает в конец списка fixated_args), и fixated_kwargs
(приписывает новые именованные аргументы и переопределяет значения старых)
и далее повторяет поведение func с этим новым набором аргументов.
3.Имеет __name__ вида partial_<имя функции func> +
4.Имеет docstring вида: +
"""
A partial implementation of <имя функции func>
with pre-applied arguements being:
<перечисление имен и значений fixated_args и fixated_kwargs>
"""
'''


def my_func(a=1, b=2, *fixated_args, **fixated_kwargs):
    print('I have fixated_args ', end='')
    print(fixated_args)
    if len(fixated_kwargs) != 0:
        print('and also fixated_kwargs ', end='')
        print(fixated_kwargs)


def partial(func, *fixated_args, **fixated_kwargs):
    def something_new(*fixated_args2, **fixated_kwargs2):
        return func(*fixated_args+fixated_args2, **{**fixated_kwargs, **fixated_kwargs2})
    something_new.__name__ = "partial_" + str(func.__name__)
    something_new.__doc__ = ("A partial implementation of " + str(func.__name__)
                             + '\nwith pre-applied arguements being:\n' +
                             str(fixated_args) + ' for fixated_args and ' +
                             str(fixated_kwargs) + ' for fixated_kwargs')
    return something_new


my_func = partial(my_func, 11, 22, qwer=111)
print(my_func.__doc__)