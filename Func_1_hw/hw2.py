'''
make_it_count, принимает функцию func, имя глобальной переменной counter_name,
возвращая новую функцию, которая ведет себя в точности как функция func,
затем исключением, что всякий раз при вызове, инкрементирует значение глобальной
переменной с именем counter_name.
'''

counter_name = 0


def func():
    print("Here we are, born to be kings\nWe're the princes of the universe")


def make_it_count(funk, name):
    def new_func(*args, **kwargs):
        globals()[name] += 1
        return funk(*args, **kwargs)
    return new_func


f = make_it_count(func, 'counter_name')
