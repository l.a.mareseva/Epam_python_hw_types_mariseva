ALTER TABLE employee
    DROP COLUMN employee_city;

ALTER TABLE employee
    ADD employee_city VARCHAR(255) NOT NULL;
    
ALTER TABLE department
    DROP COLUMN department_city;

ALTER TABLE department
    ADD department_city VARCHAR(255) NOT NULL;

DROP TABLE city CASCADE;

update db_scheme_version
set db_version = '1.4', upgraded_on = now()
