CREATE TABLE IF NOT EXISTS city (
    city_id SERIAL NOT NULL PRIMARY KEY,
    city_name VARCHAR(255) NOT NULL UNIQUE
);

update db_scheme_version
set db_version = '1.1', upgraded_on = NOW()
