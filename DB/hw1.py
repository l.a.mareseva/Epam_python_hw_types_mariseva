import psycopg2
import gzip
import csv


def depts(dest: str, connection_params: dict):
    with psycopg2.connect(**connection_params) as conn, open(dest, mode='rt') as f:
        cursor, id_this, did = conn.cursor(), 1, {}
        next(f)
        for row in f:
            dep, city = row.split(',')
            sql = """
            INSERT INTO department(department_id, department_name, 
            department_city) VALUES (%s, '%s', '%s');"""%(id_this, dep, city.strip('\n').upper())
            did[dep] = id_this
            cursor.execute(sql)
            conn.commit()
            id_this += 1
    return did


def employee(dest: str, chunk_s: int, connection_params: dict, did: dict):
    
    # bosses first
    bosses_ids = {row.split(',')[-2] for row in gzip.open(dest, mode='rt')} - {'boss', ''}
    bosses_added = set()
    with psycopg2.connect(**connection_params) as conn, gzip.open(dest, mode='rt') as f:
        cursor, boss_idS, current_chs = conn.cursor(), set(), 0
        sql_common = """
        INSERT INTO employee(employee_id, first_name, last_name, 
        employee_department, employee_city, boss, salary) VALUES """
        next(f)
        sql_values, n_subordinates, received_records = '', 0, 0
        for row in f:
            em_id, fn, ln, dep, city, boss_id, salary = row.split(',')
            if em_id == boss_id or not str(boss_id).isdigit():
                sql_values += """, (%s, '%s', '%s', %s, '%s', %s, %s)""" % (
                    em_id, fn, ln, did[dep], city, em_id, salary)
                current_chs += 1
                bosses_added.add(em_id)
            else:
                n_subordinates += 1

            if current_chs >= chunk_s:
                sql = sql_common + sql_values[1::]
                cursor.execute(sql)
                conn.commit()
                received_records += current_chs
                current_chs, sql_values = 0, ''

        if current_chs != 0:
            sql = sql_common + sql_values[1::]
            cursor.execute(sql)
            conn.commit()
            received_records += current_chs

    # dependant bosses
    with psycopg2.connect(**connection_params) as conn, gzip.open(dest, mode='rt') as f:

        bosses_req = bosses_ids - bosses_added
        cursor, current_chs, sql_values = conn.cursor(), 0, ''
        sql_common = """
        INSERT INTO employee(employee_id, first_name, last_name, 
        employee_department, employee_city, boss, salary) VALUES """
        next(f)
        for row in f:
            em_id, fn, ln, dep, city, boss_id, salary = row.split(',')
            if em_id in bosses_req:
                sql_values += """, (%s, '%s', '%s', %s, '%s', %s, %s)""" % (
                    em_id, fn, ln, did[dep], city, boss_id, salary)
                current_chs += 1

            if current_chs >= chunk_s:
                sql = sql_common + sql_values[1::]
                cursor.execute(sql)
                conn.commit()
                received_records += current_chs
                current_chs= 0
                sql_values = ''

        if current_chs != 0:
            sql = sql_common + sql_values[1::]
            cursor.execute(sql)
            conn.commit()
            received_records += current_chs

    # workers
    with psycopg2.connect(**connection_params) as conn, gzip.open(dest, mode='rt') as f:

        bosses_req = bosses_ids - bosses_added
        cursor, current_chs, sql_values = conn.cursor(), 0, ''
        sql_common = """
        INSERT INTO employee(employee_id, first_name, last_name, 
        employee_department, employee_city, boss, salary) VALUES """
        next(f)
        for row in f:
            em_id, fn, ln, dep, city, boss_id, salary = row.split(',')
            if em_id not in bosses_ids and boss_id != '':
                sql_values += """, (%s, '%s', '%s', %s, '%s', %s, %s)""" % (
                    em_id, fn, ln, did[dep], city, boss_id, salary)
                current_chs += 1

            if current_chs >= chunk_s:
                sql = sql_common + sql_values[1::]
                cursor.execute(sql)
                conn.commit()
                received_records += current_chs
                current_chs= 0
                sql_values = ''

        if current_chs != 0:
            sql = sql_common + sql_values[1::]
            cursor.execute(sql)
            conn.commit()
            received_records += current_chs

connection_params = {'host': 'localhost', 'port': '5434', 'user': 'qqq', 'password': 'qqq', 'dbname': 'homework'}

did = depts('csv/DEPTS.csv', connection_params)
employee('csv/EMPLOYEE.csv.gz', 100000, connection_params, did)