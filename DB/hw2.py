import json
import psycopg2
from threading import Thread
import time
from queue import Queue


class DBWorker(Thread):
    connection_params = {
        'host': 'localhost',
        'port': '5434',
        'dbname': 'homework',
        'user': 'qqq',
        'password': 'qqq'
    }

    def __init__(self, name, queue, responses):
        Thread.__init__(self)
        self.name = name
        self.queue = queue
        self.daemon = True
        self.responses = responses

    def run(self):
        func = self.queue.get()
        result = func(**DBWorker.connection_params)
        self.responses[func.__name__] = result
        self.queue.task_done()


def hw1(host="localhost", port="5434", dbname="homework", user="qqq", password="qqq"):
    with psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password) as conn:
        cur = conn.cursor()
        cur.execute("SELECT first_name, last_name, count(*) AS cnt\
                    FROM employee\
                    GROUP BY first_name, last_name\
                    order by cnt desc\
                    limit 1")
        conn.commit()
        result = cur.fetchall()
        return "{} {}".format(result[0][0], result[0][1])


def hw2(host="localhost", port="5434", dbname="homework", user="qqq", password="qqq"):
    with psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password) as conn:
        cur = conn.cursor()
        cur.execute("select count(*)\
                    from(SELECT employee_city, employee_department, department_city\
                    FROM employee JOIN department\
                    ON employee_department = department_id\
                    WHERE employee_city != upper(department_city)) as p;")
        conn.commit()
        return cur.fetchall()[0][0]


def hw3(host="localhost", port="5434", dbname="homework", user="qqq", password="qqq"):
    with psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password) as conn:
        cur = conn.cursor()
        cur.execute("select count(*)\
                    from(SELECT *\
                    from employee t1\
                      inner join employee t2\
                    on t1.boss=t2.employee_id\
                    where t1.salary > t2.salary) as p;")
        conn.commit()
        return cur.fetchall()[0][0]


def hw4(host="localhost", port="5434", dbname="homework", user="qqq", password="qqq"):
    with psycopg2.connect(host=host, port=port, dbname=dbname, user=user, password=password) as conn:
        cur = conn.cursor()
        cur.execute("select department_name\
                    from(select avg(salary) as avg_salary, employee_department as department\
                    from employee\
                    group by employee_department) as p\
                    inner join department\
                    on p.department = department.department_id\
                    order by avg_salary desc\
                    limit 1")
        conn.commit()
        return cur.fetchall()[0][0]



start = time.time()
funcs = [hw1, hw2, hw3, hw4]
queue, responses, workers = Queue(), {}, []
for func in funcs:
        queue.put(func)
for _ in range(queue.qsize()):
    workers.append(DBWorker(func.__name__, queue, responses))
    workers[_].start()
queue.join()
results_json = json.dumps(responses, sort_keys=True, indent=4, separators=(',', ':'))
print(results_json)
