'''
1. измерить алгоритмическую сложность реализованного в numpy
алгоритма перемножения матриц.
2. создать из полученных данных дата-фрейм pandas и вывести в виде таблицы
3. посчитать средние значения и погрешности
4. построить при помощи matplotlib график зависимости времени перемножения матриц от размера
матрицы с отображением погрешностей ("усов"), обозначений осей, подписей, легендой,
5. экспортировать график в файл, пригодный для вставки: а) в MS Word; б) в LaTex.

ас - показатель степени в соотношении, связывающем размер матриц и время, затраченное на их произведение.
матрицы N1xN1 за T1, а N2xN2 за T2.
ас n ===  (T2/T1) = (N2/N1)**n
Измерив  T1 и T2: n = log(T2/T1)/log(N2/N1).
посчитать T для разных N и построить на графике точки в логарифмическом
масштабе по обеим осям
выяснить, зависит ли n от N, и если зависит, то как.
То есть, помимо графика log(T)(log(N)) необходимо построить ещё и график n(N)
(на этот раз в обычных, нелогарифмических осях).
 На каждом графике экспериментальные
точки должны быть представлены в виде точки с погрешностями, а линия должна
аппроксимировать эти точки.
Иными словами, я ожидаю увидеть графики типа представленных на прикрепленном рисунке.

Программа должна корректно работать как под Windows, так и под Linux. Это значит,
что я ожидаю увидеть питоновский скрипт, который запускается из командной строки
и выводит графические результаты в отдельных окнах, а также записывает графики
в файлы растрового и векторного формата (скажем, png и eps).
'''
import numpy as np
import sys
import pandas as pd
from timeit import *
from math import *
import scipy as sy
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import os.path
import pylab
#%matplotlib inline


def dawn_of_matrix(i=0):
    stuffing = []
    line = [k for k in range(0, i)]
    for j in range(0, i):
        stuffing.append(line)
    a, b = np.array(stuffing), np.array(stuffing)
    start = default_timer()
    v = a * b
    stop = default_timer()
    return stop-start


def filling(wl, appearance=0):
    # size time0 time1 time2 n0 n1 n2
    maxi = max(wl)
    data_s, data_t, data_n, average_t, ac_t, average_n, ac_n = [], np.zeros((3, maxi)), np.zeros((3, maxi)), [], [], [], []
    for i in range(0, maxi):
        data_s.append(i)
        for j in range(0, 3):
            data_t[j][i] = dawn_of_matrix(i)
            if i > 0:
                N1, N2, T1, T2 = data_s[i - 1], i, data_t[j][i - 1], data_t[j][i]
                if N1 != 0 and T1 != 0 and N2 != 0 and T2 != 0 and log(N2 / N1) != 0:
                    data_n[j][i] = log(T2 / T1) / log(N2 / N1)
        average_t.append(np.mean(data_t[:, i]) or 0.00000001)
        ac_t.append(np.std(data_t[:, i]))
        average_n.append(np.mean(data_n[:, i]))
        if 1 < np.std(data_n[:, i]):
            ac_n.append(np.std(data_n[:, i])/100)
        else:
            ac_n.append(np.std(data_n[:, i]))
        if i%100 == 0:
            print('q')
    return data_t, data_n, data_s, average_t, ac_t, average_n, ac_n


file_path = "data_for_matrix.csv"
filename = 'data_for_matrix'
if os.path.exists(file_path):
    df = pd.read_csv('data_for_matrix.csv', header=0, names=['sizes', 'time0', 'time1', 'time2', 'n0', 'n1', 'n2',
                                                             'average_t', 'inaccuracy_t', 'average_n', 'inaccuracy_n'])
else:
    work_list = list(sorted({int(item) for item in np.logspace(0, 3.1)}))
    m = max(work_list)
    data_t, data_n, data_s, medi_t, ac_t, medi_n, ac_n = filling(work_list)
    data = {'sizes': data_s, 'size_for_log': data_s, 'time0': data_t[0], 'time1': data_t[1], 'time2': data_t[2], 'n0': data_n[0],
            'n1': data_n[1], 'n2': data_n[2], 'average_t': medi_t, 'inaccuracy_t': ac_t, 'average_n': medi_n, 'inaccuracy_n': ac_n}
    df = pd.DataFrame(data)
    df.to_csv("data_for_matrix.csv")
# filterd for nN
data_sl, medi_n, acc_n = np.array(df.sizes), np.array(df.average_n), np.array(df.inaccuracy_n)
mask = 2 < medi_n
medi_n, data_sl, acc_n = medi_n[mask], data_sl[mask], acc_n[mask]
mask2 = medi_n < 3
medi_n, data_sl, acc_n = medi_n[mask2], data_sl[mask2], acc_n[mask2]
print(df)
s, n = list(data_sl), list(medi_n)
pylab.figure(1)
pylab.title('The dependence of n on N')
pylab.xlabel('N')
pylab.ylabel('n')
pylab.ylim(0, 4)
fp, residuals, rank, sv, rcond = sy.polyfit(s, n, 1, full=True)
f = sy.poly1d(fp)
pylab.plot(s, f(s), linewidth=2, label="Approximation")
pylab.errorbar(s, n, yerr=list(acc_n), linestyle='None', marker='d', markersize=6, label="Experiment")
pylab.legend(loc='best', shadow=True, fontsize='medium')

pylab.savefig('The dependence of n on N.eps', dpi=300, format="eps")
pylab.savefig('The dependence of n on N.png', dpi=300, format="png")


n = list(df.sizes[1:])
t = list(df.average_t[1:])
pylab.figure(2)
pylab.title('log(T)(log(N))')
pylab.xlabel('N')
pylab.ylabel('T')
ne = np.log(n)
te = np.log(t)
acc_t = np.array(df.inaccuracy_t[1:])
p = np.polyfit(ne, te, 1)
x_fitlin = np.linspace(0, 8, 4)
y_fitlin = p[0]*x_fitlin + p[1]
x_fitlog = np.exp(x_fitlin)
y_fitlog = np.exp(y_fitlin)
pylab.loglog(x_fitlog, y_fitlog, label='Approximation')
pylab.errorbar(n, t, yerr=acc_t, linestyle='None', marker='d', markersize=2, label="Experiment")
pylab.legend(loc='best', shadow=True, fontsize='medium')

pylab.savefig('log(T)(log(N)).eps', dpi=300, format="eps")
pylab.savefig('log(T)(log(N)).png', dpi=300, format="png")
pylab.show()
'''if sys.platform == 'win32':
    plt.get_current_fig_manager().window.setGeometry(10,50,1024,768)
else:
    plt.get_current_fig_manager().window.wm_geometry("1024x768+10+50")'''
