class Calculator:
    def __init__(self, opcodes, operators=None):
        self.opcodes = opcodes
        self.operators = operators if operators is not None else []

    def pro(op):
        if op == '+':
            return 1
        elif op == '-':
            return 0
        elif op == '@':
            return 2
        elif op == '*':
            return 3
        elif op == '/':
            return 4
        elif op == '^':
            return 5
        else:
            return 6

    def __str__(self) -> str:
        stack = []
        out = []
        operators = ['+', '-', '*', '/', '^', '@']
        for i in self.opcodes:
            if i.isdigit() or i.isalpha():
                out += i
            elif i == '(':
                stack += i
            elif i == ')':
                stack = stack[::-1]
                while True:
                    j = 0
                    if stack[j] != '(':
                        out += stack[j]
                        stack.remove(stack[j])

                    elif stack[j] == '(':
                        stack.remove(stack[j])
                        stack = stack[::-1]
                        break

            elif i in operators:
                if stack == []:
                    if i == '@':
                        i = '-'
                    stack += i
                else:
                    stack = stack[::-1]
                    if i == '^':
                        stack += i
                    else:
                        k = 0
                        to_del = []
                        while (k < len(stack)) and Calculator.pro(stack[k]) <= (Calculator.pro(i)):
                            out += stack[k]
                            to_del += stack[k]
                            k += 1
                        stack = stack[::-1]
                        for k in to_del:
                            stack.remove(k)
                        if i == '@':
                            i = '-'
                        stack.append(i)
        stack = stack[::-1]
        out += stack
        o = ''
        for i in out:
            o += i
        return o

    def optimise(self):
        if self.validate():
            for operator in self.operators:
                self.opcodes = operator.process(self.opcodes)

    def validate(self) -> bool:
        # Calculator.make_polish(self)
        k = 0
        l = len(self.opcodes)
        for i in range(0, l):
            if self.opcodes[i] == '(':
                k += 1
            if self.opcodes[i] == ')':
                k -= 1
            if i+1 == l and k != 0:
                return False

        ignore = 0
        operators = ['*', '/', '^']
        if self.opcodes[0] in operators or (self.opcodes[0] == '(' and self.opcodes[1] in operators):
            return False
        opcodes = self.opcodes[::-1]
        operators = ['+', '-', '*', '/', '^']
        for i in range(0, l):
            while not(opcodes[i].isdigit() or opcodes[i].isalpha()):
                if i == 0 and opcodes[i] != ')':
                    return False
                if opcodes[i] == '/' and opcodes[i-1] == '0':
                    return False
                if opcodes[i] == '-' or opcodes[i] == '(':
                    ignore += 1
                elif not i+1 == l and not(opcodes[i+1] == '(' or opcodes[i+1]
                    == ')' or opcodes[i+1] == '-' or opcodes[i+1].isdigit() or opcodes[i+1].isalpha()):
                    return False
                if i+1 < l and opcodes[i] in operators and opcodes[i+1] in operators:
                    if not (opcodes[i] == '-' and opcodes[i+1] == '-'):
                        return False
                if not i+1 == l:
                    i += 1
                else:
                    break
        return True


class AbstractOptimiser:

    def process(self, graph):
        self.graph = graph
        self.graph = self.pre_process()
        self.graph = self.post_process(self.graph)
        return self.graph

    def pre_process(self) -> str:
        return

    def process_internal(self, graph):
        return graph

    def post_process(self, result=0):
        return result


class DoubleNegativeOptimiser(AbstractOptimiser):

    def pre_process(self) -> str:
        l = len(self.graph)
        for i in range(1, len(self.graph)):
            if i < len(self.graph) and self.graph[i] == '-':
                while True:
                    if self.graph[i-1] == '(':
                        i += 1
                    elif i+4 < l and self.graph[i+1] == '(' and self.graph[i+2] == '-' and (self.graph[i+3].isdigit() or self.graph[i+3].isalpha()) and self.graph[i+4] == ')':
                        self.graph.insert(i, '+')
                        for k in range(i+1, i+5):
                            if not (self.graph[i+1].isdigit() or self.graph[i+1].isalpha()):
                                self.graph.pop(i+1)
                            else:
                                self.graph.pop(i+2)
                        l = len(self.graph)
                        break
                    elif self.graph[i+1].isdigit() or self.graph[i+1].isalpha():
                        self.graph.insert(i, '@')
                        self.graph.pop(i+1)
                        l = len(self.graph)
                        break
                    else:
                        break
        return self.graph

    def post_process(self, result=0):
        l = len(self.graph)
        for i in range(1, l-1):
            if i < len(self.graph):
                if self.graph[i] == '-':
                    j = i
                    while True:
                        if self.graph[j-1] == '(':
                            j -=1
                        elif self.graph[j-1] == '-':
                            self.graph.pop(i)
                            self.graph.pop(j-1)
                            l -= 2
                            break
                        else:
                            break
        return self.graph


class IntegerCostantsOptimiser(AbstractOptimiser):

    def process_internal(self, graph=0):
        if self.graph[self.nums[1]] == '+':
            return int(self.graph[self.nums[0]]) + int(self.graph[self.nums[2]])
        elif self.graph[self.nums[1]] == '@':
            return int(self.graph[self.nums[0]]) - int(self.graph[self.nums[2]])
        elif self.graph[self.nums[1]] == '*':
            return int(self.graph[self.nums[0]]) * int(self.graph[self.nums[2]])
        elif self.graph[self.nums[1]] == '/':
            return int(int(self.graph[self.nums[0]]) / int(self.graph[self.nums[2]]))
        elif self.graph[self.nums[1]] == '^':
            return int(self.graph[self.nums[0]]) ** int(self.graph[self.nums[2]])
        else:
            return 0
        return graph

    def pre_process(self) -> str:
        l = len(self.graph)
        operators = ['+', '-', '*', '/', '^', '@']
        not_yet = True
        while not_yet:
            for i in range(0, l):
                if i + 1 == len(self.graph):
                    not_yet = False
                    return self.graph
                if i < len(self.graph) and self.graph[i].isdigit():

                    self.nums = []
                    extra = 0
                    self.nums.append(i)
                    while True:
                        if i < l-1 and (self.graph[i+1] == ')' or self.graph[i+1] == '('):
                            i += 1
                        elif i < l-1 and self.graph[i+1] in operators and self.graph[i+2] in operators:
                            i += 1
                            extra += 1
                        elif i < l-1 and self.graph[i+1] in operators and self.graph[i+2] not in operators:
                            self.nums.append(i+1)
                            i += 1
                            while True:
                                if i < l-1 and (self.graph[i+1] == ')' or self.graph[i+1] == '('):
                                    i += 1
                                elif self.graph[i+1].isdigit():
                                    mex = 1
                                    self.nums.append(i+1)
                                    new = str(self.process_internal())
                                    if '-' in new:
                                        mex += 1
                                        new = new.split('-')
                                        new[0] = '-'
                                    for q in range(0, 3+extra):
                                        w = self.graph.pop(self.nums[0])
                                    if mex > 1:
                                        for h in range(0, mex):
                                            self.graph.insert(self.nums[h], str(new[h]))
                                    else:
                                        self.graph.insert(self.nums[0], str(new))
                                    l = len(self.graph)
                                    break
                                break
                            break
                            #return self.graph

                        break
            #break
        return self.graph


class SimplifierOptimiser(AbstractOptimiser):

    def process_internal(self, graph=0):
    # b/b, a-a, a+(b-b)
        if (self.work_with[0] == 'a' and self.work_with[1] == 'd') or (self.work_with[0] == 'd' and self.work_with[1] == 'a'):
            if self.graph[self.nums[1]] == '+' and (self.graph[self.nums[0]] == '0' or self.graph[self.nums[2]] == '0'):
                if self.graph[self.nums[0]] == '0':
                    return self.graph[self.nums[2]]
                else:
                    return self.graph[self.nums[0]]
            elif self.graph[self.nums[1]] == '*' and (self.graph[self.nums[0]] == '0' or self.graph[self.nums[2]] == '0'):
                return 0
            elif self.graph[self.nums[1]] == '*' and (self.graph[self.nums[0]] == '1' or self.graph[self.nums[2]] == '1'):
                if self.graph[self.nums[0]] == '1':
                    return self.graph[self.nums[2]]
                else:
                    return self.graph[self.nums[0]]
            elif self.graph[self.nums[1]] == '^':
                if self.graph[self.nums[2]] == '1':
                    return self.graph[self.nums[0]]
                else:
                    return 1
            elif self.graph[self.nums[1]] == '/' and self.graph[self.nums[2]] == '1':
                return self.graph[self.nums[0]]
        if self.work_with[0] == 'a' and self.work_with[1] == 'a':
            if self.graph[self.nums[1]] == '/' and (self.graph[self.nums[0]] == self.graph[self.nums[2]]):
                return 1
            elif (self.graph[self.nums[1]] == '-' or self.graph[self.nums[1]] == '@') and (self.graph[self.nums[0]] == self.graph[self.nums[2]]):
                return 0
        else:
            return '~'

    def pre_process(self) -> str:
        operators = ['+', '-', '*', '/', '^', '@']
        not_yet = True
        while not_yet:
            l = len(self.graph)
            if l == 1:
              return self.graph
            for i in range(0, l-2):
                self.nums = []
                self.work_with = []
                if i < l-1 and (self.graph[i].isdigit() or self.graph[i].isalpha()):
                    if self.graph[i].isdigit():
                        self.work_with.append('d')
                    else:
                        self.work_with.append('a')
                    extra = 0
                    self.nums.append(i)
                    while True:
                        if i+2 == l:
                            not_yet = False
                        if i < l - 1 and (self.graph[i + 1] == ')' or self.graph[i + 1] == '('):
                            i += 1
                        elif i < l - 1 and self.graph[i + 1] in operators and self.graph[i + 2] not in operators:
                            self.nums.append(i + 1)
                            i += 1
                            while True:
                                if i + 2 == l:
                                    not_yet = False
                                if i < l - 1 and (self.graph[i + 1] == ')' or self.graph[i + 1] == '('):
                                    i += 1
                                    extra += 2
                                elif self.graph[i + 1].isdigit() or self.graph[i+1].isalpha():
                                    if self.graph[i+1].isdigit():
                                        self.work_with.append('d')
                                    else:
                                        self.work_with.append('a')
                                    self.nums.append(i+1)

                                    new = str(self.process_internal())
                                    if new == "None" or new == '~':
                                        break
                                    for q in range(0, 3 + extra):
                                        w = self.graph.pop(self.nums[0])
                                    self.graph.insert(self.nums[0], str(new))
                                    l = len(self.graph)
                                    break
                                else:
                                    break
                        else:
                            break
        return self.graph
    pass
