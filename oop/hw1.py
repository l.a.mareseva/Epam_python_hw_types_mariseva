import operator


class Calculator:
    def __init__(self, opcodes, operators=None):
        self.opcodes = opcodes
        self.operators = operators if operators is not None else []

    def pro(op):
        if op == '+':
            return 1
        elif op == '-':
            return 2
        elif op == '*':
            return 3
        elif op == '/':
            return 4
        elif op == '^':
            return 5
        else:
            return 0

    def __str__(self) -> str:
        stack = []
        out = []
        operators = ['+', '-', '*', '/', '^']
        for i in self.opcodes:
            if i.isdigit() or i.isalpha():
                out += i
            elif i == '(':
                stack += i
            elif i == ')':
                stack = stack[::-1]
                to_del = []
                l = len(stack)
                while True:
                    j = 0
                    if stack[j] != '(':
                        out += stack[j]
                        stack.remove(stack[j])

                    elif stack[j] == '(':
                        stack.remove(stack[j])
                        p = len(stack) + 1
                        stack = stack[::-1]
                        break

            elif i in operators:
                if stack == []:
                    stack += i
                else:
                    stack = stack[::-1]
                    if i == '^':
                        stack += i
                    else:
                        k = 0
                        to_del = []
                        while (k < len(stack)) and Calculator.pro(stack[k]) >= (Calculator.pro(i)):
                            if k < len(stack):
                                out += stack[k]
                                to_del += stack[k]
                            k += 1
                        stack = stack[::-1]
                        for k in to_del:
                            stack.remove(k)
                        stack.append(i)
        stack = stack[::-1]
        out += stack
        o = ''
        for i in out:
            o += i
        return o

    def optimise(self):
        if self.validate():
            for operator in self.operators:
                self.opcodes = operator.process(self.opcodes)



    def validate(self) -> bool:
        # Calculator.make_polish(self)
        k = 0
        l = len(self.opcodes)
        for i in range(0, l):
            if self.opcodes[i] == '(':
                k += 1
            if self.opcodes[i] == ')':
                k -= 1
            if i+1 == l and k != 0:
                return False

        ignore = 0
        operators = ['*', '/', '^']
        if self.opcodes[0] in operators or (self.opcodes[0] == '(' and self.opcodes[1] in operators):
            return False
        opcodes = self.opcodes[::-1]
        operators = ['+', '-', '*', '/', '^']
        for i in range(0, l):
            while not(opcodes[i].isdigit() or opcodes[i].isalpha()):
                if i == 0 and opcodes[i] != ')':
                    return False
                if opcodes[i] == '/' and opcodes[i-1] == '0':
                    return False
                if opcodes[i] == '-' or opcodes[i] == '(':
                    ignore += 1
                elif not i+1 == l and not(opcodes[i+1] == '(' or opcodes[i+1]
                    == ')' or opcodes[i+1] == '-' or opcodes[i+1].isdigit() or opcodes[i+1].isalpha()):
                    return False
                if i+1 < l and opcodes[i] in operators and opcodes[i+1] in operators:
                    if not (opcodes[i] == '-' and opcodes[i+1] == '-'):
                        return False
                if not i+1 == l:
                    i += 1
                else:
                    break
        return True



