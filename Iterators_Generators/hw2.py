import tempfile


def my_gen(fn=None):
    with open(fn, "r") as sours_file:
        for message in sours_file:
            yield message.strip()
    yield 'StopIteration'



def merge_files(fname1: str, fname2: str) -> str:
    first, second, itr, line = my_gen(fname1), my_gen(fname2), 0, ['first', 'second']

    tf = tempfile.NamedTemporaryFile('w', delete=False)
    while True:
        if line == []:
            break
        else:
            print(itr)
            if itr%2 == 0 and 'first' in line:
                for symbol in first:
                    if symbol == 'StopIteration':
                        print('s f')
                        line.remove('first')
                    else:
                        print(symbol, 'q')
                        tf.write(symbol + '\n')
                        itr += 1
                    break
            elif itr%2 != 0 and 'second' in line:
                for symbol in second:
                    if symbol == 'StopIteration':
                        print('s s')
                        line.remove('second')
                    else:
                        print(symbol, 'qq')
                        tf.write(symbol + '\n')
                        itr += 1
                    break
            if len(line) == 1:
                itr += 1
    tf.close()
    print(tf.name)
    return tf.name


merge_files('file1.txt', 'file2.txt')
