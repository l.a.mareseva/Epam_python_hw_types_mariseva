"""
E - dict(<V> : [<V>, <V>, ...])
Ключ - строка, идентифицирующая вершину графа
значение - список вершин, достижимых из данной
Реализовать итератор, совершающий обход данного графа в ширину
class Graph:
    def __init__(self, E):
        self.E = E

class GraphIterator:
    def __init__(self, graph, start_v):
        self.graph = graph
        self.start_v = start_v

    def hasNext() -> bool:
        pass

    def next() -> str:
        pass

Пример входных данных
E = {"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"], "D": ["B"], "E": ["B", "C"]}
start_v = "A"
Гарантируется, что start_v является ключом в E
"""

class Graph:
    def __init__(self, E):
        self.E = E

class GraphIterator:
    def __init__(self, graph, start):
        self.graph = graph
        self.start_v = start
        self.current_pos = start
        self.visited = list()
        self.to_visit = list()

    def hasNext(self) -> bool:
        if self.current_pos == self.start_v:
            return True
        elif self.to_visit != []:
            return True
        else:
            return False

    def next(self) -> str:
        if self.current_pos is not None:
            self.visited.append(self.current_pos)
            if self.current_pos in self.to_visit:
                self.to_visit.remove(self.current_pos)
            for i in self.graph.E[self.current_pos]:
                if i not in self.to_visit and i not in self.visited:
                    self.to_visit.append(i)
            if self.to_visit != []:
                self.current_pos = self.to_visit[0]
            else:
                if len(self.visited) == len(self.graph.E):
                    self.current_pos = None
                else:
                    for i in self.graph.E.keys():
                        if i not in self.visited:
                            self.current_pos = i
                            self.to_visit.append(i)
            print(self.visited, self.to_visit)
            return self.visited[len(self.visited)-1]

        else:
            raise StopIteration


E = {"A": ["B", "C"], "B": ["A", "C", "D", "E"], "C": ["A", "B", "E"],"D": ["B",], "E": ["B", "C"], "F":[]}
start_v = "A"
my_graph = Graph(E)
x = GraphIterator(my_graph, start_v)
i = 0
while x.hasNext():
    print(x.next())
