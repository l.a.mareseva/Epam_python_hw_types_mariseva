def opn(data, loc=0):
    expi = []
    while True:
        if data[loc] == '(':
            pass
        elif data[loc] == ')':
            if loc+1 < len(data) and ((data[loc+1] == '*' or data[loc+1] == '/')
               or (data[loc-5] == '*' or data[loc-5] == '/')):
                if data[loc-2] == '+' or data[loc-2] == '-':
                    a = expi.pop(len(expi) - 1)
                    b = expi.pop(len(expi) - 1)
                    c = expi.pop(len(expi) - 1)
                    expi = expi + ['('] + [c] + [b] + [a] + [')']
            else:
                pass
        else:
            expi += data[loc]

        loc += 1
        if loc >= len(data):
            break
    return expi


def brackets_trim(input_data: str) -> str:
    clear = opn(input_data)
    result = ''
    for i in clear:
        result += i
    return result
