import stat
from os import stat
from pathlib import Path


def hardlink_check(directory_path: str) -> bool:
	with Path(directory_path) as p:
		files = [x for x in p.iterdir() if x.is_file()]
		for file1 in files:
			for file2 in files:
				if file1 is not file2:
					a, b = stat(file1), stat(file2)
					if (a.st_ino, a.st_dev) == \
						(b.st_ino, b.st_dev):
						return True
					else:
						continue
	return False