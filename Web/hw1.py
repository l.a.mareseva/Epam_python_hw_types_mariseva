from flask import Flask
from requests import get
from bs4 import BeautifulSoup
import re

app = Flask('__main__')


def get_page_text(soup):
    text = ' '.join([
        s for s in soup.strings
    ])
    return text


def put_trademark(words, text):
    for word in list(words):
        text = text.replace(word, word +'\u2122'.encode('utf-8').decode('utf-8'))
    return text


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def proxy(path):
    source_code = get('{}{}'.format('http://flask.pocoo.org/', path)).content
    soup = BeautifulSoup(source_code, 'html.parser')
    page_text = get_page_text(soup)
    bag_of_words = re.sub(r'[^\w]', ' ', page_text)
    long_words = {w for w in bag_of_words.split(' ') if len(w) >= 10}
    new_html = put_trademark(long_words, source_code.decode())
    data = new_html.encode()
    return data


app.run(host='0.0.0.0', port=8000)
