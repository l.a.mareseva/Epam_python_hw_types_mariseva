class EnumMeta(type):
    def __new__(mcls, name, bases, dct):     # определить метод '__new__'
        # Создал хранилище для пар энумов
        enum_storage, edict_storage, back_dict = {}, {}, {}
        for item, value in dct.items():     # Наполнил  хранилища  значениями
            if not (item.startswith('__') and item.endswith('__')):
                edict_storage[item] = value
                back_dict[value] = item
        # обновим
        dct.update({'enum_storage': enum_storage, 'edict_storage': edict_storage,
                    'back_dict': back_dict, 'name': name})
        Enum = super().__new__(mcls, name, bases, dct)  # родительский '__new__' и готовый класс
        #  Создал в соотвествии с данными из хранилищ экземпляры Enum
        all_enums = set()
        initialization = 0
        for item, value in edict_storage.items():
            enum_storage[item] = Enum(value)  # класс из п.3 с значением
            setattr(Enum, item, enum_storage[item])

        return Enum

    def __getitem__(cls, key):
        if key in cls.__dict__.keys():
            return cls.__dict__[key]
        else:
            raise KeyError(key)

    def __iter__(cls):
        return iter(cls.enum_storage)


class Enum(metaclass=EnumMeta):
    '''
    Реализация класса Enum, подобная существующей в Python
    '''
    def __new__(cls, value):
        # 5.Мы инициализируем класс с помощью какого то значения  (0, 90, 180 и т.д.).
        # 5.1 Если значение не фигурирует в коллекциях из п.1 - кидаем ошибку (нет такого енума)
        if value not in cls.back_dict.keys():
            raise ValueError('no enum')
        # пытаемся найти уже существеующий экземпляр, его же и возвращаем.
        elif cls.back_dict[value] in cls.enum_storage.keys():
            return cls.enum_storage[cls.back_dict[value]]
        # 5.3 создаем объект - указываем в качестве класса Enum (атрибут '__сlass__'), указываем атрибуты name и value.
        else:
            new_attributes = {'name': cls.back_dict[value], 'value': value, 'c_name': cls.__class__}
            cls.enum_storage[cls.back_dict[value]] = object.__new__(__class__)
            for item, val in new_attributes.items():
                setattr(cls.enum_storage[cls.back_dict[value]], item, val)
            return cls.enum_storage[cls.back_dict[value]]   # 5.4 Возвращаем свежеиспеченный экземпляр.

    def __str__(self):
        return '{}.{}: {}'.format(self.c_name, self.name, self.value)

class Direction(Enum):
    north = 0
    east = 90
    south = 180
    west = 270


print(Direction)
print(Enum.__dict__)


#1
print(Direction.north)  # <Direction.north: 0>
print(Direction.south)  # <Direction.south: 180>
print(Direction.north.name)  # north
print(Direction.north.value)  # 0 

#2
for d in Direction:
    print(d)
# Direction.north
# Direction.east
# Direction.south
# Direction.west

#3
print(id(Direction.north))  # 2171479819208
print(id(Direction(0)))  # 2171479819208

#4
print(Direction['west'])  # <Direction.west: 270=0>
print(Direction['north-west'])  # KeyError: 'north-west'
