﻿class OurDescriptor(object):

    def __init__(self, name='standart name'):
        self.name = name

    def __get__(self):
        print('Work with ', self.name)
        return self.name

    def __set__(self, obj, val):
        assert val >= 0, "non-negative value required"
        print('Set', self.name)
        self.val = val


class BePositive(object):
    some_value = OurDescriptor('some')
    another_value = OurDescriptor('another')


instance = BePositive()
# должно работать
instance.some_value = 1
# должно выкинуть ошибку
instance.another_value = -1
