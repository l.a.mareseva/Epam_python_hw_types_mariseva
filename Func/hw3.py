def collatz_steps(n):
    steps = 0

    def another_step(s):
        return s + 1

    def sugar_del(n):
        nonlocal steps
        steps = another_step(steps)
        res = [int(x / 2) == 1 and int(x * 3 + 1) or (int(x / 2) % 2 == 0 and sugar_del(int(x / 2)) or sugar_inc(int(x/ 2))) for x in range(n, n + 1)]
        return int(res[0])

    def sugar_inc(n):
        nonlocal steps
        steps = another_step(steps)
        res = [int(x*3 + 1) == 1 and int(x*3 + 1) or (int(x*3 + 1) % 2 == 0 and sugar_del(int(x*3 + 1)) or sugar_inc(int(x*3 + 1))) for x in range(n, n+1)]
        return int(res[0])

    '''def del_(n):
        nonlocal steps
        steps = another_step(steps)
        return int(n / 2)

    def inc_(n):
        nonlocal steps
        steps = another_step(steps)
        return int(n*3 + 1) '''

    new = [x % 2 == 0 and sugar_del(x) or sugar_inc(x) for x in range(n, n+1) if n != 1]
    return steps


print(collatz_steps(16)) # == 4
print(collatz_steps(12)) # == 9
print(collatz_steps(1000000)) # == 152