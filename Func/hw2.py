from functools import reduce


def is_armstrong(number):
    new_check = lambda x, y: x**y
    check_value = reduce(lambda x, y: x + y, [new_check(int(i), len(str(int(number)))) for i in list(str(number))])
    return check_value == number


assert is_armstrong(153) == True, 'Число Армстронга'
print(is_armstrong(9))
print(is_armstrong(10))
print(is_armstrong(153))