'''
Получить сумму чисел являющихся полными квадратами натуральных чисел и меньших 10^6. Решить тремя способами
'''
from functools import reduce
import math


def summ1():  # простой
    summ = 0
    for i in range(1, 1000):
        summ += i**2
    return summ


def summ2():  # с использованием reduce
    nums = []
    for i in range(1, 1000):
        nums.append(i**2)
    return reduce(lambda x, y: x + y, nums)


def summ3():
    return reduce(lambda x, y: x + y, list(map(lambda x: x*x, range(1, 1000))))


print(summ1())
print(summ2())
print(summ3())
