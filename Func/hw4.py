'''
Решения должны быть максимально лаконичными, и использовать list comprehensions, reduce, etc.
problem40 - list comprehension, reduces
'''
from functools import reduce
import operator
import math


def Pythagorean_triplet():
    return int([x*y*int(math.sqrt(x**2 + y**2)) for x in range(1, 999) for y in range(1, 999) if (x+y+math.sqrt(x**2 + y**2) == 1000) and (math.sqrt(x**2 + y**2) - int(math.sqrt(x**2 + y**2)) == 0) and (x < y < int(math.sqrt(x**2 + y**2)))][0])


def Sum_square_difference(): #24174150
    return reduce(lambda x, y: x-y, [reduce(lambda x, y: x**2+y, [reduce(lambda x, y: x+y,list(map(lambda x: x, range(1, 101)))), 0]), reduce(lambda x, y: x+y, list(map(lambda x: x * x, range(1, 101))))])


def Self_powers(): # 9110846700
    return str(reduce(lambda x, y: x+y, list(map(lambda x: x**x, range(1, 1000)))))[-10:]


def Champernownes_constant():
    s = ''.join([str(x) for x in range(0, 1000000)])
    return reduce(lambda x,y: x*y, [int(s[10 ** i]) for i in range(0, 7)])


print(Champernownes_constant())





