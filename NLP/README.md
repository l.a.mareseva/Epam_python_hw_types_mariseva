# Отчёт

### Тест №1: Naive Bayes classifier (no lemmatization)

```
                        precision    recall  f1-score   support

           alt.atheism       0.97      0.60      0.74       319
         comp.graphics       0.96      0.89      0.92       389
               sci.med       0.97      0.81      0.88       396
soc.religion.christian       0.65      0.99      0.78       398

           avg / total       0.88      0.83      0.84      1502
```

### Тест №2: Naive Bayes classifier (lemmatized)

```
                        precision    recall  f1-score   support

           alt.atheism       0.98      0.47      0.63       319
         comp.graphics       0.96      0.88      0.92       389
               sci.med       0.93      0.83      0.88       396
soc.religion.christian       0.61      0.99      0.76       398

           avg / total       0.86      0.81      0.80      1502
```

### Тест №3: Support Vector Machine (lemmatized)

```
                        precision    recall  f1-score   support

           alt.atheism       0.89      0.75      0.81       319
         comp.graphics       0.81      0.97      0.88       389
               sci.med       0.92      0.82      0.87       396
soc.religion.christian       0.88      0.90      0.89       398

           avg / total       0.87      0.87      0.87      1502

```

# Вывод

При переходе к лемматизированной версии Naive Bayes наблюдается незначительное общее снижение средних значений точности. Вероятно это обусловлено наличием в заголовках файлов обучающей и тестовой выборки информации о сообщении и его авторе. Добиться улучшения точности можно с использованием алгоритма SVM.