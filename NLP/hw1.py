import sklearn.datasets
from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
import spacy
from io import StringIO


class LemmaTokenizer(object):
    def __init__(self):
        self.nlp = spacy.load('en', disable=['parser', 'tagger'])
    def __call__(self, doc):
        sp_data = self.nlp(doc)
        return [token.lemma_ for token in sp_data]

#Loading Data
categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']
#twenty_train = sklearn.datasets.load_files('20news-bydate-train', categories=categories, encoding='latin1')
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=42)

#Building a pipeline
text_clf = Pipeline([('vect', CountVectorizer(tokenizer=LemmaTokenizer())),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier(loss='hinge', penalty='l2',
        alpha=1e-3, random_state=42,
        max_iter=5, tol=None)),
])

text_clf.fit(twenty_train.data, twenty_train.target)

# Evaluate performance
twenty_test = fetch_20newsgroups(subset='test', categories=categories, shuffle=True, random_state=42)
docs_test = twenty_test.data

text_clf.fit(twenty_train.data, twenty_train.target)
predicted = text_clf.predict(docs_test)
print(np.mean(predicted == twenty_test.target))

print(metrics.classification_report(twenty_test.target, predicted,
    target_names=twenty_test.target_names))